## TASK1-INFO2300

**Reason for choosing the license**

1. We decided to go with MIT license because it is permissive license and it is the most popular license.

## Using NPM Install

```sh
npm install task1-sample
```

## Using Yarn

```sh
yarn add task1-sample
```
